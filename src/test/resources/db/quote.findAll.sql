INSERT INTO persona (id, first_name, last_name)
VALUES
        (1, 'Arthur', 'Pendragon'),
        (2, 'Guenièvre', 'De Carmélide');

INSERT INTO quote (id, body, author_id)
VALUES
       (1, 'Bohort, je vous donne l''ordre de vous rendre immédiatement en Andalousie pour y rencontrer le chef wisigoth et lui transmettre le message de paix suivant :... « Coucou »...', 1),
       (2, 'Ah non, avec les jolies seulement. C’est de là que j’ai conclu que, comme y m’touchait pas, je faisais moi-même partie des grosses mochetés.', 2);
